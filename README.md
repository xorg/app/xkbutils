xkbutils is a collection of small utilities utilizing the X Keyboard (XKB)
extension to the X11 protocol.

It includes:

 * xkbbell  - generate X Keyboard Extension bell events
 * xkbvleds - display X Keyboard Extension LED state in a window
 * xkbwatch - report state changes using the X Keyboard Extension

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/app/xkbutils

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

